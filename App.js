import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Provinsiku from "./provinsi";
import Kabupatenku from "./kabupaten";
import Kecamatan from "./kecamatan";
import Kelurahan from "./kelurahan";
import { createStackNavigator, createAppContainer } from "react-navigation";
import { registerRootComponent } from "expo";
import HomeScreen from './App/index';

const MainStack = createStackNavigator(
  {
    Home: {
        screen: HomeScreen,
        navigationOptions: {
            header: null
        }
    },
    Provinsi: {
      screen: Provinsiku,
      navigationOptions: {
        headerTitle: "Provinsi",
        headerTintColor: "#fff",
        headerStyle: {
          backgroundColor: "red"
        }
      }
    },
    Kabupaten: {
      screen: Kabupatenku,
      navigationOptions: {
        headerTitle: "Kabupaten",
        headerTintColor: "#fff",
        headerStyle: {
          backgroundColor: "red"
        }
      }
    },
    Kecamatan: {
      screen: Kecamatan,
      navigationOptions: {
        headerTitle: "Kecamatan",
        headerTintColor: "#fff",
        headerStyle: {
          backgroundColor: "red"
        }
      }
    },
    Kelurahan: {
      screen: Kelurahan,
      navigationOptions: {
        headerTitle: "Kelurahan",
        headerTintColor: "#fff",
        headerStyle: {
          backgroundColor: "red"
        }
      }
    }
      
  },
  {
    initialRouteName: "Home"
  }
);

export default registerRootComponent(createAppContainer(MainStack));
