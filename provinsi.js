import React, { Component } from "react";
import { FlatList, StyleSheet, Text, View, Button, StatusBar } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

const styles = StyleSheet.create({
  container: {
    alignItems: "flex-start",
      justifyContent: "flex-start",
      padding: 2
  },
  header:{
      fontSize: 25,
      color: "#826251"
  },
  provButton:{
      fontWeight: "bold",
      color: "red",
      marginTop: 2,
      borderBottomWidth: 2,
      borderColor: "red",
      padding: 10,
      borderRadius: 20,
      width: "100%"
  },
  FlatList:{
    width: "100%"
  }
});

class Utama extends Component {
  state = {
    data: []
  };

  constructor(props){
    super(props)

  }

  componentWillMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch(
      "http://dev.farizdotid.com/api/daerahindonesia/provinsi"
    );
    const json = await response.json();
    this.setState({ data: json.semuaprovinsi });
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={styles.FlatList}
          data={this.state.data}
          keyExtractor={item => item.id}
          renderItem={({ item }) => 
          <TouchableOpacity>
          <Text 
            onPress={() => this.props.navigation.navigate('Kabupaten', { propinsiId: item.id}) }
            color= "#568EA3"
            style={styles.provButton}
           >{item.nama}</Text>
           </TouchableOpacity>}
        />
      </View>
    );
  }
}

export default Utama;
