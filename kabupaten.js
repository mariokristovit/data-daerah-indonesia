import React, { Component } from "react";
import { FlatList, StyleSheet, Text, View,Image, Button, StatusBar } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from 'react-native-vector-icons/MaterialIcons';

const styles = StyleSheet.create({
  container: {
    alignItems: "flex-start",
      justifyContent: "flex-start",
      padding: 2
  },
  header:{
      fontSize: 25,
      color: "#826251"
  },
  provButton:{
      fontWeight: "bold",
      color: "red",
      marginTop: 2,
      borderBottomWidth: 2,
      borderColor: "red",
      padding: 10,
      borderRadius: 20,
      width: "100%"
  },
  FlatList:{
    width: "100%"
  },
  home:{
    width: 40,
    height: 40,
    // flex:1,
    alignItems:"center",
    justifyContent: "center",
    marginLeft: 2
    
  },
  lingkaran:{
    position: "absolute",
    borderRadius: 25,
    bottom: 0,
    right:0,
    width:50,
    height: 50,
    backgroundColor: "red"
  },
  rumahku:{
    alignItems:"center",
    justifyContent:"center",
  }
});

class Kabupaten extends Component {
  state = {
    data: []
  };

  constructor(props){
    super(props)
      
  }

  componentWillMount() {
    this.fetchData();
  }

  fetchData = async () => {
    let propinsiId = this.props.navigation.getParam('propinsiId')
    const response = await fetch(
      `http://dev.farizdotid.com/api/daerahindonesia/provinsi/${propinsiId}/kabupaten`
    );
    const json = await response.json();
    this.setState({ data: json.kabupatens });
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={styles.FlatList}
          data={this.state.data}
          keyExtractor={item => item.id}
          renderItem={({ item }) =>
            <TouchableOpacity>
              <Text 
            onPress={() => this.props.navigation.navigate('Kecamatan', { kabupatenId: item.id}) }
            color= "#568EA3"
            style={styles.provButton}
            >{item.nama}</Text>
            </TouchableOpacity>        
            }
        />
        <View style={styles.lingkaran} >
        <Icon onPress= {() => this.props.navigation.navigate('Provinsi')} style={styles.rumahku} name={"home"} size={50} color="#fff"></Icon>
        </View>
      </View>
    );
  }
}

export default Kabupaten;
