import React from "react";
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  SafeAreaView,
  ImageBackground,
  TouchableOpacity
} from "react-native";

export default App = () => {
  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
        <Image
          source={require("./assets/gambar2.jpg")}
          style={{ width: 98, height: 22 }}
        />
      </View>

      <View></View>

      <View style={styles.tabBar}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  navBar: {
    height: 55,
    backgroundColor: "white",
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  rightNav: {
    flexDirection: "row"
  },
  navItem: {
    marginLeft: 25
  },
  tabBar: {
    backgroundColor: "white",
    height: 60,
    borderTopWidth: 0.5,
    borderColor: "#E5E5E5",
    flexDirection: "row",
    justifyContent: "space-around"
  },
  tabItem: {
    alignItems: "center",
    justifyContent: "center"
  },
  tabTitle: {
    fontSize: 11,
    color: "#3c3c3c",
    paddingTop: 4
  },
  body: {
    flex: 1
  }
});
