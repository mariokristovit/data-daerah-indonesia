import React, { Component } from "react";
import { StyleSheet, Text, View, Image, ImageBackground } from "react-native";
import { Button, buttonContainer } from "./Component/ui1/Button";

export default App = () => (
      <View style={styles.container}>
        <ImageBackground
          source={require("./assets/merah.jpg")}
          style={{ width: "100%", height: "100%" }}
        >
          <View style={styles.inner}>
            <View style={styles.inner2}>
              <Text style={styles.textinner}></Text>
            </View>
          </View>
          <View>
            <View style={styles.textcontainer}>
              <Button text="Search" />
              <Button text="About" />
            </View>
          </View>
        </ImageBackground>
      </View>
    );


const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  navBar: {
    height: 80,
    backgroundColor: "white",
    elevation: 3,
    paddingHorizontal: 50,
    paddingTop: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  inner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  inner2: {
    backgroundColor: "white",
    height: 100,
    width: 100,
    justifyContent: "center",
    alignItems: "center"
  },
  textinner: {
    fontWeight: "bold",
    fontSize: 26
  },
  textcontainer: {
    height: 70,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    borderTopColor: "#e8e8ec",
    borderWidth: 1,
    paddingHorizontal: 25,
    flexDirection: "row"
  }
});
